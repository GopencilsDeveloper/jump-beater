﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

#if AADOTWEEN
using DG.Tweening;
#endif

namespace AppAdvisory.JumpJumpJump
{
    public class CubeElement : MonoBehaviour
    {
        public GameObject diamond;
        public GameObject diamondParticle;
        public GameObject invisible;

        public bool isRightCube;

        public GameObject BLOCK;
        public GameObject TURN_LEFT;
        public GameObject TURN_RIGHT;

        bool isScale = true;

        float timeAnimCubePosition = 0.3f;
        float timeAnimCubeScale = 0.29f;

        public void ActivateDiamond()
        {
            diamond.SetActive(true);
        }

        public void DOCollectDiamond()
        {
            gameManager.point++;
            Destroy(diamond);
            diamondParticle.transform.eulerAngles = new Vector3(Util.GetRandomNumber(0f, 180f), diamondParticle.transform.eulerAngles.y, diamondParticle.transform.eulerAngles.z);
            diamondParticle.gameObject.SetActive(true);
            diamondParticle.transform.parent = null;
        }

        public void SetInvisible()
        {
            DODesactivateShapes();
            Destroy(BLOCK);
            Destroy(TURN_LEFT);
            Destroy(TURN_RIGHT);
            Destroy(diamond);
            invisible.SetActive(true);
        }

        Renderer currentRenderer
        {
            get
            {
                if (BLOCK.activeInHierarchy)
                    return BLOCK.GetComponent<Renderer>();
                if (TURN_LEFT.activeInHierarchy)
                    return TURN_LEFT.GetComponent<Renderer>();

                return TURN_RIGHT.GetComponent<Renderer>();
            }
        }

        Collider currentCollider
        {
            get
            {
                return currentRenderer.GetComponent<Collider>();
            }
        }

        Material currentMaterial
        {
            get
            {
                return currentRenderer.material;
            }
        }

        public float decalY = 50;

        public void AddToDecal(float plus)
        {
            decalY += plus;
        }

        public SHAPE shape;

        GameManager gameManager;

        void Awake()
        {
            DODesactivateShapes();

            gameManager = FindObjectOfType<GameManager>();

            currentCollider.enabled = false;
            BLOCK.GetComponent<Collider>().enabled = false;
            TURN_LEFT.GetComponent<Collider>().enabled = false;
            TURN_RIGHT.GetComponent<Collider>().enabled = false;
        }

        void DODesactivateShapes()
        {
            BLOCK.SetActive(false);
            TURN_LEFT.SetActive(false);
            TURN_RIGHT.SetActive(false);
        }

        public void activateElement(SHAPE shape)
        {
            this.shape = shape;

            DODesactivateShapes();

            if (shape == SHAPE.BLOCK)
                BLOCK.SetActive(true);
            else if (shape == SHAPE.TURN_LEFT)
                TURN_LEFT.SetActive(true);
            else
                TURN_RIGHT.SetActive(true);
        }

        void DOAnimIn(Action isCompleted)
        {
            currentCollider.enabled = false;
#if AADOTWEEN
            if (isScale)
            {
                transform.localScale = Vector3.zero;
                transform.DOScale(Vector3.one, timeAnimCubeScale)
                    .OnComplete(() =>
                    {
                        if (isCompleted != null)
                            isCompleted();
                    });
            }

            Vector3 finalPos = transform.position;
            transform.position = new Vector3(finalPos.x, finalPos.y + decalY, finalPos.z);
            transform.DOMove(finalPos, timeAnimCubePosition)
                .OnComplete(() =>
                {
                    currentCollider.enabled = true;
                    if (isCompleted != null)
                        isCompleted();
                });
#endif
        }

        void DOAnimOut(Action isCompleted)
        {
            currentCollider.enabled = false;

#if AADOTWEEN
            if (isScale)
            {
                transform.DOScale(Vector3.zero, timeAnimCubeScale)
                    .OnComplete(() =>
                    {
                        if (isCompleted != null)
                            isCompleted();
                    });
            }
            transform.DOMove(new Vector3(transform.position.x, transform.position.y - decalY, transform.position.z), timeAnimCubePosition)
                .OnComplete(() =>
                {
                    if (isCompleted != null)
                        isCompleted();
                });
#endif
        }

        void Start()
        {
            DOAnimIn(null);
        }

        public void DOAnimOut(float waitTIme)
        {
#if AADOTWEEN
            DOVirtual.DelayedCall(waitTIme, () =>
            {
                DOAnimOut(() =>
                {
                    Destroy(gameObject);
                });
            });
#endif
        }
    }
}