﻿
/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

#if AADOTWEEN
using DG.Tweening;
#endif

namespace AppAdvisory.JumpJumpJump
{
	public class Player : MonoBehaviour 
	{
		public GameObject smokeParticle;

		public void DOSmokeParticle()
		{
			var go = Instantiate(smokeParticle) as GameObject;
			go.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
			
			go.SetActive(true);
		}

		void OnTriggerEnter(Collider other) 
		{
			if(other.name.CompareTo("Diamond") == 0)
			{
				CubeElement ce = other.GetComponentInParent<CubeElement>();
				ce.DOCollectDiamond();
			}
			else if(other.name.CompareTo("Invisible") == 0)
			{
				GetComponent<Collider>().enabled = false;
				FindObjectOfType<GameManager>().DOGameOverFall();
			}
			else
			{
				FindObjectOfType<GameManager>().DOGameOver();
			}
		}
	}
}