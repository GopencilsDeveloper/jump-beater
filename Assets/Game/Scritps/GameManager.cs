﻿/***********************************************************************************************************
 * Produced by App Advisory	- http://app-advisory.com													   *
 * Facebook: https://facebook.com/appadvisory															   *
 * Contact us: https://appadvisory.zendesk.com/hc/en-us/requests/new									   *
 * App Advisory Unity Asset Store catalog: http://u3d.as/9cs											   *
 * Developed by Gilbert Anthony Barouch - https://www.linkedin.com/in/ganbarouch                           *
 ***********************************************************************************************************/

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
#if AADOTWEEN
using DG.Tweening;
#endif
using AppAdvisory.UI;

#if UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
using NaughtyAttributes;
#endif
#if APPADVISORY_ADS
using AppAdvisory.Ads;
#endif
#if VS_SHARE
using AppAdvisory.SharingSystem;
#endif
#if APPADVISORY_LEADERBOARD
using AppAdvisory.social;
#endif

namespace AppAdvisory.JumpJumpJump
{
    public class GameManager : MonoBehaviour
    {
        #region Singleton
        private static GameManager instance;

        public static GameManager Instance
        {
            get
            {
                return instance == null ? GameObject.FindObjectOfType<GameManager>() : instance;
            }
        }
        #endregion

        public int numberOfPlayToShowInterstitial = 5;

        public string VerySimpleAdsURL = "http://u3d.as/oWD";

        public Transform cubePrefab;

        int numMaxOfCubeInScene
        {
            get
            {
                return /* minNumOfCubeInLine + */ maxNumOfCubeInLine + 2;
            }
        }


        [SerializeField] public int m_point;
        public int point
        {
            get
            {
                return m_point;
            }
            set
            {
                if (textPoint == null)
                    textPoint = FindObjectOfType<UIController>().scoreIngame;

                m_point = value;

                textPoint.text = "" + m_point;

                if (value > 0)
                    PlaySoundFX_Point();
            }
        }

        public bool CanFloorUp
        {
            get
            {
                return canFloorUp;
            }

            set
            {
                canFloorUp = value;
            }
        }

        public int minNumOfCubeInLine = 2;
        public int maxNumOfCubeInLine = 12;

        public float playerSpeed = 0.1f;

        public Color[] shapeColors;

        public Material platformMaterial;
        Player player;

        List<CubeElement> nextTargets;
        int x = 0;
        int z = 0;
        bool canFloorUp = false;

#if AADOTWEEN
        Tweener tw;
#endif
        float playerJumpTime = 0.15f;

        float playerJumpHeight = 2f;
        Vector3 lastPosCubeElement = Vector3.one * -1;
        Text textPoint;
        public AudioClip FX_Start;
        public AudioClip FX_Colision;
        public AudioClip FX_Point;
        public AudioClip FX_Reward;
        public AudioClip FX_NewCharacter;
        public AudioClip FX_Jump;
        public AudioSource audioSource;
        public void PlaySoundFX_Start()
        {
            audioSource.PlayOneShot(FX_Start);
        }
        public void PlaySoundFX_Colision()
        {
            audioSource.PlayOneShot(FX_Colision);
        }
        public void PlaySoundFX_Point()
        {
            audioSource.PlayOneShot(FX_Point);
        }
        public void PlaySoundFX_Reward()
        {
            audioSource.PlayOneShot(FX_Reward);
        }
        public void PlaySoundFX_NewCharacter()
        {
            audioSource.PlayOneShot(FX_NewCharacter);
        }
        public void PlaySoundFX_Jump()
        {
            audioSource.PlayOneShot(FX_Jump);
        }
        void InitializeSound()
        {
            StartCoroutine(COInitializeSound());
        }

        IEnumerator COInitializeSound()
        {
            audioSource.volume = 0;
            audioSource.mute = true;
            yield return null;
            audioSource.PlayOneShot(FX_Start, 0);
            audioSource.PlayOneShot(FX_Colision, 0);
            audioSource.PlayOneShot(FX_Point, 0);
            audioSource.PlayOneShot(FX_Reward, 0);
            audioSource.PlayOneShot(FX_NewCharacter, 0);
            // audioSource.PlayOneShot(FX_Jump, 0);
            yield return null;
            audioSource.volume = 1;
            audioSource.mute = false;
        }

        public void DOChangeColor()
        {
            Color c = shapeColors[UnityEngine.Random.Range(0, shapeColors.Length)];

#if AADOTWEEN
            platformMaterial.DOColor(c, 1).SetDelay(5).OnComplete(DOChangeColor);
#endif
        }

        int GetNumberOfCubeInTheScene()
        {
            var allCubes = FindObjectsOfType<CubeElement>();
            return allCubes.Length;
        }

        CubeElement InstantiateCube(int x, int z)
        {
            var t = Instantiate(cubePrefab) as Transform;
            var c = t.GetComponent<CubeElement>();
            t.position = new Vector3(-x, lastPosCubeElement.y, -z);
            currentCube = c;
            return c;
        }

        void Awake()
        {
            nextTargets = new List<CubeElement>();
            player = FindObjectOfType<Player>();

            Util.CleanMemory();

            var go = GameObject.Find("[Dotween]");

#if AADOTWEEN
            if (go == null)
                DOTween.Init();
#endif

            point = 0;

            audioSource = GetComponent<AudioSource>();

            FindObjectOfType<UIController>().SetBestText(Util.GetBestScore());
            FindObjectOfType<UIController>().SetLastText(Util.GetLastScore());

            Color c = shapeColors[UnityEngine.Random.Range(0, shapeColors.Length)];

            platformMaterial.color = c;

            DOChangeColor();
        }

        void Start()
        {
            InitializeSound();
            StartCoroutine(CreateWorld());
        }

        public void DOGameOver()
        {
            InputTouch.OnTouchedDown -= OnTouchedDown;

            Util.SetLastScore(point);

            FindObjectOfType<UIController>().SetBestText(Util.GetBestScore());
            FindObjectOfType<UIController>().SetLastText(Util.GetLastScore());

            // ShowAds();

#if VS_SHARE
			VSSHARE.DOTakeScreenShot();
#endif

#if APPADVISORY_LEADERBOARD
			LeaderboardManager.ReportScore(point);
#endif

            InputTouch.OnTouchedDown -= OnTouchedDown;

#if AADOTWEEN
            DOTween.KillAll();
#endif

            PlaySoundFX_Colision();

#if AADOTWEEN
            Camera.main.DOOrthoSize(Camera.main.orthographicSize * 0.9f, 0.1f)
                .SetDelay(0.1f)
                .SetLoops(5, LoopType.Yoyo)
                .OnComplete(() =>
                {
                    Camera.main.DOOrthoSize(Camera.main.orthographicSize * 0.01f, 1f)
                        .OnComplete(() =>
                        {
                            Util.ReloadLevel();
                        });
                });
#endif
        }

        void OnEnable()
        {
            InputTouch.OnTouchedDown -= OnTouchedDown;
        }

        void OnDisable()
        {
            InputTouch.OnTouchedDown -= OnTouchedDown;
        }

        void OnTouchedDown()
        {
            DoPlayerJump();
        }

        public int jumpCount = 0;

        void DoPlayerJump()
        {
            jumpCount++;
            if (jumpCount <= 2)
            {
                player.GetComponent<Collider>().enabled = false;

                float savePosY = player.transform.position.y;

#if AADOTWEEN
                tw.Kill(false);
#endif

                player.DOSmokeParticle();
#if AADOTWEEN
                tw = player.transform.DOMoveY(savePosY + playerJumpHeight, playerJumpTime)
                    .SetEase(Ease.OutQuad)
                    .OnComplete(() =>
                    {
                        tw = player.transform.DOMoveY(nextTargets[0].transform.position.y + 0.75f, playerJumpTime)
                            .SetEase(Ease.InQuad)
                            .OnComplete(OnJumpCompleted);
                    });
#endif
            }
        }

        void OnJumpCompleted()
        {
            player.GetComponent<Collider>().enabled = true;
            jumpCount = 0;
        }

        Vector3 diffCamPlayer;

        public void OnClickedStart()
        {
            KoreoManager.Instance.isPlaying = true;
#if VS_SHARE
			VSSHARE.DOHideScreenshotIcon();
#endif

            player.gameObject.SetActive(true);

            PlaySoundFX_Start();

            Camera.main.transform.parent = null;

            var savePos = player.transform.position;

            player.transform.position = savePos + Vector3.up * 10;

#if AADOTWEEN
            player.transform.DOMove(savePos, 1)
                .SetEase(Ease.OutBounce)
                .OnComplete(() =>
                {

                    Camera.main.transform.parent = player.transform;
                    Camera.main.transform.parent = null;

                    this.diffCamPlayer = player.transform.position - Camera.main.transform.position;

                    InputTouch.OnTouchedDown -= OnTouchedDown;
                    InputTouch.OnTouchedDown += OnTouchedDown;

                    DOPlayerMove();
                });
#endif
        }

#if AADOTWEEN
        Tweener tweenerPlayerMoveX = null;
        Tweener tweenerPlayerMoveZ = null;
#endif
        Vector3 lastTargetPosition = -1f * Vector3.one;

        void DOPlayerMove()
        {
#if AADOTWEEN
            if (tweenerPlayerMoveX != null)
                tweenerPlayerMoveX.Kill(false);

            if (tweenerPlayerMoveZ != null)
                tweenerPlayerMoveZ.Kill(false);

            var target = new Vector3(nextTargets[0].transform.position.x, player.transform.position.y, nextTargets[0].transform.position.z);

            if (lastTargetPosition.z == target.z)
            {
                tweenerPlayerMoveX = player.transform.DOMoveX(target.x, playerSpeed)
                    .SetEase(Ease.Linear)
                    .OnUpdate(OnUpdatePlayerMove)
                    .OnComplete(OnCompletePlayerMove);
            }
            else
            {
                tweenerPlayerMoveZ = player.transform.DOMoveZ(target.z, playerSpeed)
                    .SetEase(Ease.Linear)
                    .OnUpdate(OnUpdatePlayerMove)
                    .OnComplete(OnCompletePlayerMove);
            }

            lastTargetPosition = target;
#endif
        }

        void OnUpdatePlayerMove()
        {
            Vector3 tempCamPos = player.transform.position - this.diffCamPlayer;
            tempCamPos.y = lastPosCubeElement.y - this.diffCamPlayer.y;

            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, tempCamPos, 3 * Time.deltaTime);
        }

        void OnCompletePlayerMove()
        {
            nextTargets[0].DOAnimOut(playerSpeed * 5);
            nextTargets.RemoveAt(0);
            DOPlayerMove();
        }

        public void DOGameOverFall()
        {
            InputTouch.OnTouchedDown -= OnTouchedDown;
#if AADOTWEEN
            player.transform.DOMoveY(player.transform.position.y - 10, 2).OnComplete(() =>
            {
                DOGameOver();
            });
#endif
        }

        public void DoPositionSeperateZ(CubeElement c)
        {
            if (canFloorUp)
            {
                canFloorUp = false;
                z++;
            }
        }

        public void DoPositionSeperateX(CubeElement c)
        {
            if (canFloorUp)
            {
                canFloorUp = false;
                x++;
            }
        }

        public void DOPositionFloorUp(CubeElement c)
        {
            if (canFloorUp)
            {
                Vector3 v = c.transform.position;
                v.y += 0.5f;
                lastPosCubeElement = v;
                canFloorUp = false;
            }
        }

        float probaDiamond = 0f;

        bool DOActivateDiamond(CubeElement c)
        {
            if (Mathf.Abs(x) < 3 || Mathf.Abs(z) < 3)
                return false;

            if (Util.GetRandomNumber(0, 100) < probaDiamond)
            {
                c.ActivateDiamond();

                probaDiamond = 0f;

                return true;
            }
            else
            {
                probaDiamond += 1f;

                return false;
            }
        }

        float probaInvisible = 0f;

        bool DOInvisible(CubeElement c)
        {
            return false;
        }

        CubeElement currentCube;

        IEnumerator CreateWorld()
        {
            bool firstActivation = true;

            while (true)
            {
                int stopZ = maxNumOfCubeInLine + z;

                CubeElement c = null;

                bool firstPiece = true;

                while (z <= stopZ)
                {
                    while (GetNumberOfCubeInTheScene() > numMaxOfCubeInScene)
                    {
                        yield return null;
                    }

                    c = InstantiateCube(x, z);

                    if (UnityEngine.Random.value < 0.5f)
                    {
                        DoPositionSeperateZ(c);
                    }
                    else
                    {
                        DOPositionFloorUp(c);
                    }
                    // DOPositionFloorUp(c);
                    // DoPositionSeperateZ(c);
                    DOActivateDiamond(c);

                    c.AddToDecal(lastPosCubeElement.y);

                    nextTargets.Add(c);

                    c.isRightCube = true;

                    z++;

                    if (firstActivation)
                    {
                        c.activateElement(SHAPE.BLOCK);
                        firstActivation = false;
                    }
                    else
                    {
                        if (firstPiece)
                        {
                            c.activateElement(SHAPE.TURN_LEFT);
                            firstPiece = false;
                        }
                        else
                        {
                            c.activateElement(SHAPE.BLOCK);
                        }
                    }

                    if (x < 3 && z < 3)
                        c.activateElement(SHAPE.BLOCK);

                    yield return new WaitForSeconds(playerSpeed);
                }

                int stopX = maxNumOfCubeInLine + x;

                firstPiece = true;

                while (x <= stopX)
                {
                    while (GetNumberOfCubeInTheScene() > numMaxOfCubeInScene)
                    {
                        yield return null;
                    }

                    c = InstantiateCube(x, z);

                    if (UnityEngine.Random.value < 0.5f)
                    {
                        DoPositionSeperateX(c);
                    }
                    else
                    {
                        DOPositionFloorUp(c);
                    }

                    // DOPositionFloorUp(c);
                    // DoPositionSeperateX(c);
                    DOActivateDiamond(c);

                    c.AddToDecal(lastPosCubeElement.y);

                    nextTargets.Add(c);

                    c.isRightCube = false;

                    x++;

                    if (firstActivation)
                    {
                        c.activateElement(SHAPE.BLOCK);
                        firstActivation = false;
                    }
                    else
                    {
                        if (firstPiece)
                        {
                            c.activateElement(SHAPE.TURN_RIGHT);
                            firstPiece = false;
                        }
                        else
                        {
                            c.activateElement(SHAPE.BLOCK);
                        }
                    }

                    yield return new WaitForSeconds(playerSpeed);
                }
            }
        }

        /// <summary>
        /// If using Very Simple Ads by App Advisory, show an interstitial if number of play > numberOfPlayToShowInterstitial: http://u3d.as/oWD
        /// </summary>
        public void ShowAds()
        {
            int count = PlayerPrefs.GetInt("GAMEOVER_COUNT", 0);
            count++;
            PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
            PlayerPrefs.Save();

#if APPADVISORY_ADS
			if(count > numberOfPlayToShowInterstitial)
			{
				print("count = " + count + " > numberOfPlayToShowINterstitial = " + numberOfPlayToShowInterstitial);

				if(AdsManager.instance.IsReadyInterstitial())
				{
					print("AdsManager.instance.IsReadyInterstitial() == true ----> SO ====> set count = 0 AND show interstial");
					AdsManager.instance.ShowInterstitial();
					PlayerPrefs.SetInt("GAMEOVER_COUNT",0);
				}
				else
				{
#if UNITY_EDITOR
					print("AdsManager.instance.IsReadyInterstitial() == false");
#endif
				}

			}
			else
			{
				PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
			}
			PlayerPrefs.Save();
#else
            if (count >= numberOfPlayToShowInterstitial)
            {
                Debug.LogWarning("To show ads, please have a look to Very Simple Ad on the Asset Store, or go to this link: " + VerySimpleAdsURL);
                Debug.LogWarning("Very Simple Ad is already implemented in this asset");
                Debug.LogWarning("Just import the package and you are ready to use it and monetize your game!");
                Debug.LogWarning("Very Simple Ad : " + VerySimpleAdsURL);
                PlayerPrefs.SetInt("GAMEOVER_COUNT", 0);
            }
            else
            {
                PlayerPrefs.SetInt("GAMEOVER_COUNT", count);
            }
            PlayerPrefs.Save();
#endif
        }

    }
}