﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;
using AppAdvisory.JumpJumpJump;

[System.Serializable]
public class Koreo
{
    public string eventID;
    public Koreography koreoGraphy;
}

public class KoreoManager : MonoBehaviour
{
    #region Singleton
    private static KoreoManager instance;
    public static KoreoManager Instance
    {
        get
        {
            return instance == null ? GameObject.FindObjectOfType<KoreoManager>() : instance;
        }
    }
    #endregion

    public float leadInTime;
    float leadInTimeLeft;
    float timeLeftToPlay;

    [Header("List Koreo")]
    [SerializeField]
    public Koreo[] koreos;

    private AudioSource audioSource;
    private Koreography playingKoreo;
    private SimpleMusicPlayer smp;
    private string eventID;
    private KoreographyTrackBase rhythmTrack;
    private List<KoreographyEvent> rawEvents;
    private int pendingIndex = 0;
    private int sampleRate;
    public bool onEvent;
    public bool isPlaying = false;

    #region Properties
    public int DelayedLatestSampleTime
    {
        get
        {
            return playingKoreo.GetLatestSampleTime() - (int)(audioSource.pitch * leadInTimeLeft * sampleRate);
        }
    }

    #endregion

    private void Awake()
    {
        smp = GetComponent<SimpleMusicPlayer>();
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop();
    }

    private void Start()
    {
        InitializeLeadIn();
        InitializeKoreo(0);
        isPlaying = false;
    }

    private void Update()
    {
        CountDownToPlayAudio();
        OnReachEvent();
    }

    public void InitializeKoreo(int index)
    {
        playingKoreo = koreos[index].koreoGraphy;
        eventID = koreos[index].eventID;

        smp.LoadSong(playingKoreo, 0, false);
        rhythmTrack = playingKoreo.GetTrackByID(eventID);
        rawEvents = rhythmTrack.GetAllEvents();
        sampleRate = playingKoreo.SampleRate;
    }

    void InitializeLeadIn()
    {
        // Initialize the lead-in-time only if one is specified.
        if (leadInTime > 0f)
        {
            // Set us up to delay the beginning of playback.
            leadInTimeLeft = leadInTime;
            timeLeftToPlay = leadInTime - Koreographer.Instance.EventDelayInSeconds;
        }
        else
        {
            // Play immediately and handle offsetting into the song.  Negative zero is the same as
            //  zero so this is not an issue.
            audioSource.time = -leadInTime;
            audioSource.Play();
        }
    }

    void CountDownToPlayAudio()
    {
        if (isPlaying)
        {
            // Count down some of our lead-in-time.
            if (leadInTimeLeft > 0f)
            {
                leadInTimeLeft = Mathf.Max(leadInTimeLeft - Time.unscaledDeltaTime, 0f);
            }

            // Count down the time left to play, if necessary.
            if (timeLeftToPlay > 0f)
            {
                timeLeftToPlay -= Time.unscaledDeltaTime;

                // Check if it is time to begin playback.
                if (timeLeftToPlay <= 0f)
                {
                    audioSource.time = -timeLeftToPlay;
                    audioSource.Play();
                    timeLeftToPlay = 0f;
                }
            }
        }
    }

    public void OnReachEvent()
    {
        while (pendingIndex < rawEvents.Count && rawEvents[pendingIndex].StartSample < DelayedLatestSampleTime)
        {
            GameManager.Instance.CanFloorUp = true;
            pendingIndex++;
        }
    }
}
